<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');
Route::get('/getting-started', 'HomeController@start');
Route::post('/start', 'HomeController@starting');

Auth::routes();

Route::resource('narudžbenice', 'NarudzbenicaController')->middleware('auth');
Route::resource('fakture', 'FakturaController')->middleware('auth');
Route::resource('otpremnice', 'OtpremnicaController')->middleware('auth');
Route::resource('roba', 'RobaController')->middleware('auth');
Route::resource('grupa-robe', 'GrupaRobeController')->middleware('auth');
Route::resource('pdv', 'PdvController')->middleware('auth');
Route::resource('cenovnici', 'CenovnikController')->middleware('auth');
Route::put('cenovnici/aktivan/{cenovnik}', 'CenovnikController@toggleActive')->middleware('auth')->name('cenovnici.active');

Route::get('faktura/download/{faktura}', 'FakturaController@download')->name('faktura.download');
