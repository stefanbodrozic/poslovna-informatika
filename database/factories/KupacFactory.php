<?php

use App\Kupac;
use Faker\Generator as Faker;

$factory->define(Kupac::class, function (Faker $faker) {
    return [
        'naziv' => $faker->name,
        'adresa' => $faker->streetAddress,
        'pib' => $faker->numberBetween($min = 100000, $max = 90000),
        'jmbg' => $faker->numberBetween($min = 100000, $max = 90000),
        'preduzece_id' => 1,
        'mesto_id' => $faker->numberBetween($min = 1, $max = 50)
    ];
});
