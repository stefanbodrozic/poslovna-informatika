<?php

use App\Prevoznik;
use Faker\Generator as Faker;

$factory->define(Prevoznik::class, function (Faker $faker) {
    return [
        'naziv' => $faker->company
    ];
});
