<?php

use App\Mesto;
use Faker\Generator as Faker;

$factory->define(Mesto::class, function (Faker $faker) {
    return [
        'grad' => $faker->city,
        'postanski_broj' => $faker->postcode
    ];
});
