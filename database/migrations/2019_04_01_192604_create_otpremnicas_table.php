<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpremnicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otpremnice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('broj');
            $table->boolean('roba_primljena')->default(false);
            $table->decimal('ukupan_iznos', 8, 2);
            $table->unsignedInteger('kupac_id')->nullable();
            $table->unsignedInteger('prevoznik_id');
            $table->unsignedInteger('preduzece_id')->nullable();
            $table->unsignedInteger('faktura_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('poslovna_godina_id');
            $table->dateTime('datum_porudzbine');
            $table->dateTime('datum_isporuke');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('kupac_id')->references('id')->on('kupci');
            // $table->foreign('prevoznik_id')->references('id')->on('prevoznici');
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otpremnicas');
    }
}
