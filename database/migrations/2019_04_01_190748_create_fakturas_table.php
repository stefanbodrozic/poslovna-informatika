<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fakture', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('broj');
            $table->decimal('ukupan_iznos', 8, 2);
            $table->decimal('ukupan_porez', 8, 2);
            $table->decimal('ukupan_rabat', 8, 2);
            $table->decimal('rabat', 5, 2);
            $table->unsignedInteger('kupac_id');
            $table->unsignedInteger('preduzece_id');
            $table->unsignedInteger('prevoznik_id');
            $table->unsignedInteger('narudzbenica_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('poslovna_godina_id');
            $table->dateTime('datum_fakture');
            $table->dateTime('datum_isporuke');
            $table->dateTime('platiti_do');
            $table->text('napomena')->nullable();
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('pdv_id')->references('id')->on('pdv');
            // $table->foreign('kupac_id')->references('id')->on('kupci');
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
            // $table->foreign('narudzbenica_id')->references('id')->on('narudzbenice');
            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fakturas');
    }
}
