<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoslovnaGodinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('poslovne_godine', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('godina');
            $table->dateTime('vazi_od');
            $table->dateTime('vazi_do');
            $table->unsignedInteger('preduzece_id');
            $table->boolean('zakljucena')->default(false);

            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('poslovna_godinas');
    }
}
