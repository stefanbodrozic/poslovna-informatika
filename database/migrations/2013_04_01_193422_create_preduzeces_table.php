<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePreduzecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preduzeca', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naziv');
            $table->string('pib')->unique();
            $table->string('adresa');
            $table->string('email')->unique();
            $table->string('telefon');
            $table->string('racun');
            $table->text('potpis');
            $table->text('logo');
            $table->unsignedInteger('mesto_id');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('mesto_id', 'preduzece_mesto_id')->references('id')->on('mesta');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preduzeces');
    }
}
