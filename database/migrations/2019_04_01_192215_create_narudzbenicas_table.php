<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNarudzbenicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narudzbenice', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('broj')->nullable();
            $table->unsignedInteger('kupac_id')->nullable();
            $table->unsignedInteger('prevoznik_id');
            $table->unsignedInteger('preduzece_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('poslovna_godina_id');
            $table->decimal('rabat', 5, 2);
            $table->text('napomena')->nullable();
            $table->dateTime('datum_isporuke');
            $table->dateTime('datum_porudzbine');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('kupac_id')->references('id')->on('kupci');
            // $table->foreign('prevoznik_id')->references('id')->on('prevoznici');
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
            // $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('narudzbenicas');
    }
}
