<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRobasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roba', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naziv');
            $table->string('jedinica_mere');
            $table->unsignedInteger('grupa_robe_id');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('grupa_robe_id')->references('id')->on('grupe_robe');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robas');
    }
}
