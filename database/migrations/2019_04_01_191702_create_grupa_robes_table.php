<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrupaRobesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grupe_robe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naziv')->unique();
            $table->unsignedInteger('pdv_id');
            $table->unsignedInteger('preduzece_id');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('pdv_id')->references('id')->on('pdv');
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupa_robes');
    }
}
