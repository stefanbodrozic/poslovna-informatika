<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKupacsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kupci', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naziv');
            $table->string('adresa');
            $table->string('pib')->nullable();
            $table->string('jmbg')->nullable();
            $table->unsignedInteger('mesto_id');
            $table->unsignedInteger('preduzece_id');
            $table->softDeletes();
            $table->timestamps();

            // $table->foreign('mesto_id')->references('id')->on('mesta');
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kupacs');
    }
}
