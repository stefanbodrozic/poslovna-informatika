<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFakturaStavkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faktura_stavka', function (Blueprint $table) {
            $table->unsignedInteger('faktura_id');
            $table->unsignedInteger('stavka_id');
            $table->integer('kolicina');
            $table->decimal('cena', 8, 2);
            $table->decimal('vrednost', 8, 2);
            $table->decimal('rabat_iznos', 8, 2);
            $table->decimal('osnovica_pdv', 8, 2);
            $table->decimal('stopa_pdv', 8, 2);
            $table->decimal('iznos_pdv', 8, 2);
            $table->decimal('prodajna_vrednost', 8, 2);

            // $table->foreign('faktura_id')->references('id')->on('fakture');
            // $table->foreign('stavka_id')->references('id')->on('roba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faktura_stavkas');
    }
}
