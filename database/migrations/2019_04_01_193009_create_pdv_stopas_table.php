<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePdvStopasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pdv_stope', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('procenat', 5, 2);
            $table->boolean('aktivan')->default(1);
            $table->unsignedInteger('pdv_id');
            $table->dateTime('vazi_do');
            $table->timestamps();

            // $table->foreign('pdv_id')->references('id')->on('pdv');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pdv_stopas');
    }
}
