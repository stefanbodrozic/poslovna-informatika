<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenovnikStavkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cenovnik_stavka', function (Blueprint $table) {
            $table->unsignedInteger('cenovnik_id');
            $table->unsignedInteger('stavka_id');
            $table->decimal('cena', 8, 2);

            // $table->foreign('cenovnik_id')->references('id')->on('cenovnici');
            // $table->foreign('stavka_id')->references('id')->on('roba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cenovnik_stavkas');
    }
}
