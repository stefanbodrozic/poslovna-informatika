<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCenovniksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cenovnici', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('naziv')->unique();
            $table->unsignedInteger('preduzece_id');
            $table->boolean('aktivan')->default(1);
            // $table->dateTime('vazi_do');
            $table->softDeletes();
            $table->timestamps();
            
            // $table->foreign('preduzece_id')->references('id')->on('preduzeca');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cenovniks');
    }
}
