<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOtpremnicaStavkaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otpremnica_stavka', function (Blueprint $table) {
            $table->unsignedInteger('otpremnica_id');
            $table->unsignedInteger('stavka_id');
            $table->integer('kolicina');
            $table->decimal('prodajna_vrednost', 8, 2);

            // $table->foreign('otpremnica_id')->references('id')->on('otpremnice');
            // $table->foreign('stavka_id')->references('id')->on('roba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('otpremnica_stavka', function (Blueprint $table) {
            //
        });
    }
}
