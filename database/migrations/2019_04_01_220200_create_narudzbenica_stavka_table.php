<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNarudzbenicaStavkaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('narudzbenica_stavka', function (Blueprint $table) {
            $table->unsignedInteger('narudzbenica_id');
            $table->unsignedInteger('stavka_id');
            $table->integer('kolicina');

            // $table->foreign('narudzbenica_id')->references('id')->on('narudzbenice');
            // $table->foreign('stavka_id')->references('id')->on('roba');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('narudzbenica_stavka', function (Blueprint $table) {
            //
        });
    }
}
