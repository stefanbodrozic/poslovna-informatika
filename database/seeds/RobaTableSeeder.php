<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RobaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roba')->insert([
            'naziv' => 'mleko',
            'jedinica_mere' => 'l',
            'grupa_robe_id' => 2,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roba')->insert([
            'naziv' => 'hleb',
            'jedinica_mere' => 'kg',
            'grupa_robe_id' => 1,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roba')->insert([
            'naziv' => 'jabuka',
            'jedinica_mere' => 'kg',
            'grupa_robe_id' => 1,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roba')->insert([
            'naziv' => 'tastatura',
            'jedinica_mere' => 'g',
            'grupa_robe_id' => 3,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);

        DB::table('roba')->insert([
            'naziv' => 'monitor',
            'jedinica_mere' => 'g',
            'grupa_robe_id' => 3,
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);
    }
}
