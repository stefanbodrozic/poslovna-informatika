<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CenovnikStavkaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 1,
            'stavka_id' => 1,
            'cena' => 2.10
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 1,
            'stavka_id' => 2,
            'cena' => 4.30
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 1,
            'stavka_id' => 3,
            'cena' => 5.20
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 1,
            'stavka_id' => 4,
            'cena' => 118.80
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 1,
            'stavka_id' => 5,
            'cena' => 213.20
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 2,
            'stavka_id' => 1,
            'cena' => 1.90
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 2,
            'stavka_id' => 2,
            'cena' => 4.10
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 2,
            'stavka_id' => 3,
            'cena' => 5.00
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 2,
            'stavka_id' => 4,
            'cena' => 114.00
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 2,
            'stavka_id' => 5,
            'cena' => 200.60
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 3,
            'stavka_id' => 1,
            'cena' => 1.80
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 3,
            'stavka_id' => 2,
            'cena' => 4.00
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 3,
            'stavka_id' => 3,
            'cena' => 5.30
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 3,
            'stavka_id' => 4,
            'cena' => 100
        ]);

        DB::table('cenovnik_stavka')->insert([
            'cenovnik_id' => 3,
            'stavka_id' => 5,
            'cena' => 190
        ]);
    }
}
