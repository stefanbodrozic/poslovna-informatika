<?php

use App\Mesto;
use Illuminate\Database\Seeder;

class MestaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Mesto::class, 50)->create();
    }
}
