<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MestaTableSeeder::class);
        $this->call(PreduzecaTableSeeder::class);
        $this->call(PoslovneGodineTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GrupeRobeTableSeeder::class);
        $this->call(PrevoznikTableSeeder::class);
        $this->call(PdvTableSeeder::class);
        $this->call(PdvStopeTableSeeder::class);
        $this->call(CenovnikTableSeeder::class);
        $this->call(KupciTableSeeder::class);
        $this->call(RobaTableSeeder::class);
        $this->call(CenovnikStavkaTableSeeder::class);
    }
}
