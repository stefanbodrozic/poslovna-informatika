<?php

use Illuminate\Database\Seeder;

class PdvStopeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pdv_stope')->insert([
            'procenat' => 15.50,
            'aktivan' => 1,
            'pdv_id' => 1,
            'vazi_do' => '2022-04-11 04:15:14',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('pdv_stope')->insert([
            'procenat' => 22.30,
            'aktivan' => 1,
            'pdv_id' => 2,
            'vazi_do' => '2022-04-11 04:15:14',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
        DB::table('pdv_stope')->insert([
            'procenat' => 30.20,
            'aktivan' => 1,
            'pdv_id' => 3,
            'vazi_do' => '2022-04-11 04:15:14',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
