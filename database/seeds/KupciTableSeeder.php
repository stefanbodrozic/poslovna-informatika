<?php

use App\Kupac;
use Illuminate\Database\Seeder;

class KupciTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Kupac::class, 20)->create();
    }
}
