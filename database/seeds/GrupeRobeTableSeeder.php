<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrupeRobeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('grupe_robe')->insert([
            'naziv' => 'hrana',
            'pdv_id' => 1,
            'preduzece_id' => 1,
        ]);

        DB::table('grupe_robe')->insert([
            'naziv' => 'piće',
            'pdv_id' => 2,
            'preduzece_id' => 1,
        ]);

        DB::table('grupe_robe')->insert([
            'naziv' => 'tehnika',
            'pdv_id' => 3,
            'preduzece_id' => 1,
        ]);
    }
}
