<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PreduzecaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('preduzeca')->insert([
            'naziv' => 'Bodiroga i sinovi',
            'pib' => '3196428',
            'adresa' => 'Bulevar revolucije 16',
            'email' => 'preduzece@gmail.com',
            'telefon' => '+387654278411',
            'racun' => '7598 8573187 5318 314',
            'potpis' => 'img/potpisi/bodiroga-i-sinovi.png',
            'logo' => 'img/logo/bodiroga-i-sinovi.png',
            'mesto_id' => 1,
        ]);
    }
}
