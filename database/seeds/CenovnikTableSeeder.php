<?php

use Illuminate\Database\Seeder;

class CenovnikTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cenovnici')->insert([
            'naziv' => 'Cenovnik 3',
            'preduzece_id' => 1,
            'aktivan' => 1,
            'created_at' =>  '2019-03-01 00:00:00',
            'updated_at' => '2019-03-01 00:00:00',
        ]);

        DB::table('cenovnici')->insert([
            'naziv' => 'Cenovnik 2',
            'preduzece_id' => 1,
            'aktivan' => 0,
            'created_at' =>  '2019-02-01 00:00:00',
            'updated_at' => '2019-02-01 00:00:00',
        ]);

        DB::table('cenovnici')->insert([
            'naziv' => 'Cenovnik 1',
            'preduzece_id' => 1,
            'aktivan' => 0,
            'created_at' =>  '2019-01-01 00:00:00',
            'updated_at' => '2019-01-01 00:00:00',
        ]);
    }
}
