<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PoslovneGodineTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('poslovne_godine')->insert([
            'godina' => '2017',
            'vazi_od' => '2017-02-01 00:00:00',
            'vazi_do' => '2018-02-01 00:00:00',
            'preduzece_id' => 1,
            'zakljucena' => 1
        ]);

        DB::table('poslovne_godine')->insert([
            'godina' => '2018',
            'vazi_od' => '2018-02-01 00:00:00',
            'vazi_do' => '2019-02-01 00:00:00',
            'preduzece_id' => 1,
            'zakljucena' => 1
        ]);

        DB::table('poslovne_godine')->insert([
            'godina' => '2019',
            'vazi_od' => '2019-02-01 00:00:00',
            'vazi_do' => '2020-02-01 00:00:00',
            'preduzece_id' => 1,
            'zakljucena' => 0
        ]);
    }
}
