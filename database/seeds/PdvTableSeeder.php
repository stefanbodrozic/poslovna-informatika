<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PdvTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pdv')->insert([
            'naziv' => 'PDV 1',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('pdv')->insert([
            'naziv' => 'PDV 2',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);

        DB::table('pdv')->insert([
            'naziv' => 'PDV 3',
            'created_at' =>  \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now(),
        ]);
    }
}
