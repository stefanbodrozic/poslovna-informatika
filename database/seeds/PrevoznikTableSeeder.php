<?php

use App\Prevoznik;
use Illuminate\Database\Seeder;

class PrevoznikTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Prevoznik::class, 5)->create();
    }
}
