@extends('layouts.master')
@section('title', __('Novi cenovnik'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Kopirate cenovnik: ') . $stari_cenovnik->naziv }}</h3>

            <div class="ks-controls">

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Novi cenovnik') }}</h5>
                                    <form action="{{ route('cenovnici.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="aktivan-input" class="col-sm-3 form-control-label">{{ __('Aktivan') }}</label>
                                            <div class="col-sm-9">
                                                <label class="ks-checkbox-slider ks-on-off ks-solid ks-primary">
                                                    <input
                                                        type="checkbox"
                                                        name="aktivan"
                                                        id="aktivan-input"
                                                        value="1"
                                                    >
                                                    <span class="ks-indicator"></span>
                                                    <span class="ks-on">Da</span>
                                                    <span class="ks-off">Ne</span>
                                            </label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="naziv-input" class="col-sm-3 form-control-label">{{ __('Naziv') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="naziv"
                                                    class="form-control {{ $errors->has('naziv') ? 'is-invalid' : '' }}"
                                                    id="naziv-input"
                                                    placeholder="{{ __('Unesite naziv') }}"
                                                    value="{{ old('naziv') }}"
                                                >
                                                @if($errors->has('naziv'))
                                                    <small class="text-danger">{{ $errors->first('naziv') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        {{-- <div class="form-group row">
                                            <label for="vazi-do-input" class="col-sm-3 form-control-label">{{ __('Važi do') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="datetime-local"
                                                    min="2019-01-01T00:00" 
                                                    max="2030-01-14T00:00"
                                                    step="1"
                                                    name="vazi_do"
                                                    class="form-control {{ $errors->has('vazi_do') ? 'is-invalid' : '' }}"
                                                    id="vazi-do-input"
                                                    placeholder="{{ __('Unesite datum važenja') }}"
                                                    value="{{ old('vazi_do') }}"
                                                >
                                                @if($errors->has('vazi_do'))
                                                    <small class="text-danger">{{ $errors->first('vazi_do') }}</small>
                                                @endif
                                            </div>
                                        </div> --}}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 ks-panels-column-section">
                            <div class="card">
                                <div class="ks-body-wrap-container">
                                    <div class="ks-full-table">
                                        <div class="ks-full-table-header">
                                            <h4 class="ks-full-table-name">
                                                {{ __('Roba cenovnika: ') . $stari_cenovnik->naziv }}
                                            </h4>
                                        </div>
                                        <table id="roba-table" class="table ks-table-info dt-responsive nowrap">
                                            <thead>
                                                <tr>
                                                    <th width="1">ID </th>
                                                    <th>{{ __('Naziv') }}</th>
                                                    <th>{{ __('$ Stara cena') }}</th>
                                                    <th>{{ __('$ Nova cena') }}</th>
                                                    <th>{{ __('Jedinica mere') }}</th>
                                                    <th>{{ __('Grupa robe') }}</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-lg-12">
                                    <button id="cenovnik-submit" class="btn btn-primary">
                                        {{ __('Napravite cenovnik') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#roba-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"{{ route('cenovnici.show', $stari_cenovnik) }}", 
            columns: [
                { data: "id" }, 
                { data: "naziv" },
                { 
                    data: "cenovnici",
                    render: function (data) {
                        return data[0].pivot.cena;
                    }
                },
                { 
                    data: "cenovnici",
                    render: function (data, type, row, meta) {
                        return '<input class="cenovnik-roba form-control col-lg-4" type="number" data-id="' + row.id + '" step="0.05" value="' + row.cenovnici[0].pivot.cena + '">';
                    }
                },
                { data: "jedinica_mere" },
                { data: "grupa_robe.naziv" },
            ]
        })
    }
};

$(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('#cenovnik-submit').on('click', function (e) {
        e.preventDefault();

        var roba = [];
        $('.cenovnik-roba').each(function (i, obj) {
            let id = $(obj).data('id');
            let cena = obj.value;

            let stavka = {
                id: id,
                cena: cena
            };

            roba.push(stavka);
        });

        var aktivan = 0;
        if ($('#aktivan-input').is(':checked')) {
            aktivan = 1;
        }
        
        var naziv = $('#naziv-input').val();

        var data = {
            aktivan: aktivan,
            naziv: naziv,
            roba: roba
        };

        $.ajax({
            url: '{{ route('cenovnici.store') }}',
            type: 'POST',
            data: data,
            success: function (data) {
                let url = '{{ route('cenovnici.index') }}';
                window.location.replace(url);
            },
            error: function (data) {
                alert('error');
            }
        });
    });
});
</script>
@endsection
