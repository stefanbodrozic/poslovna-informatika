<div class="ks-controls">
    <a href="{{ route('cenovnici.show', $cenovnik) }}">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-eye ks-icon"></span>
        </button>
    </a>
    <a href="{{ route('cenovnici.edit', $cenovnik) }}">
        <button type="button" class="btn btn-success ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-copy ks-icon"></span>
        </button>
    </a>
    @if($cenovnik->aktivan)
        <button type="button" class="toggle-active btn btn-outline-primary ks-light ks-no-text" data-id="{{ $cenovnik->id }}" data-active="true" style="line-height: 38px;">
            <span class="la la-toggle-on ks-icon"></span>
        </button>
    @else
        <button type="button" class="toggle-active btn btn-outline-danger ks-light ks-no-text" data-id="{{ $cenovnik->id }}" data-active="false" style="line-height: 38px;">
            <span class="la la-toggle-off ks-icon"></span>
        </button>
    @endif
</div>
