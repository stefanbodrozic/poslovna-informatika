<div class="ks-controls">
    <a href="{{ route('pdv.edit', ['pdv' => $pdv]) }}" target="_blank">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-plus ks-icon"></span>
        </button>
    </a>
</div>
