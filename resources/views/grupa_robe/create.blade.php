@extends('layouts.master')
@section('title', __('Nova grupa robe'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Nova grupa robe') }}</h3>

            <div class="ks-controls">

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Nova grupa robe') }}</h5>
                                    <form action="{{ route('grupa-robe.store') }}" method="POST">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="naziv-input" class="col-sm-3 form-control-label">{{ __('Naziv') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="text"
                                                    name="naziv"
                                                    class="form-control {{ $errors->has('naziv') ? 'is-invalid' : '' }}"
                                                    id="naziv-input"
                                                    placeholder="{{ __('Unesite naziv') }}"
                                                    value="{{ old('naziv') }}"
                                                >
                                                @if($errors->has('naziv'))
                                                    <small class="text-danger">{{ $errors->first('naziv') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pdv-input" class="col-sm-3 form-control-label">
                                                {{ __('PDV') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    name="pdv_id"
                                                    id="pdv-input"
                                                    class="form-control"
                                                >
                                                    <option disabled selected>Odaberite pdv</option>
                                                    @foreach($pdvs as $pdv)
                                                        <option value="{{ $pdv->id }}">{{ $pdv->naziv . ', ' . $pdv->aktivnaStopa->procenat . ' %' }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('pdv_id'))
                                                    <small class="text-danger">{{ $errors->first('pdv_id') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {

});
</script>
@endsection
