@extends('layouts.master')
@section('title', __('Nova narudzbenica'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Nova narudzbenica') }}</h3>

            <div class="ks-controls">

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-nav-body-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-6 ks-panels-column-section">
                            <div class="card">
                                <div class="card-block">
                                    <h5 class="card-title">{{ __('Nova narudzbenica') }}</h5>
                                    <form action="{{ route('narudžbenice.store') }}" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="roba-input" class="col-sm-3 form-control-label">
                                                {{ __('Roba') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <div id="stavka-forms">
                                                    <div class="form-inline">
                                                        <input 
                                                            type="number"
                                                            class="form-control col-sm-2 {{ $errors->has('roba_kolicina.*') ? 'is-invalid' : '' }}" 
                                                            name="roba_kolicina[]"
                                                            value="1"
                                                            step="1"
                                                        >
                                                        <select 
                                                            name="roba_id[]" 
                                                            id="roba-input"
                                                            class="form-control col-sm-10 {{ $errors->has('roba_id') ? 'is-invalid' : '' }}" 
                                                        >
                                                            <option selected disabled>Izaberite robu</option>
                                                            @foreach($roba as $stavka)
                                                                <option value="{{ $stavka->id }}">
                                                                    {{ $stavka->naziv . ', ' . $stavka->grupaRobe->naziv . ', ' . $stavka->aktivnaCena() . ' $' }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('roba_kolicina.*'))
                                                            <small class="text-danger">{{ $errors->first('roba_kolicina.*') }}</small>
                                                        @endif
                                                        @if($errors->has('roba_id'))
                                                            <small class="text-danger">{{ $errors->first('roba_id') }}</small>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="form-inline" id="append-forms"></div>
                                                <button class="btn btn-primary pull-right col-sm-2 mt-2" id="add-new-form">+</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="kupac-input" class="col-sm-3 form-control-label">
                                                {{ __('Kupac') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    name="kupac_id"
                                                    id="kupac-input"
                                                    class="form-control"
                                                >
                                                    <option disabled selected>Odaberite kupca</option>
                                                    @foreach($kupci as $kupac)
                                                        <option 
                                                            value="{{ $kupac->id }}"
                                                            {{ old('kupac_id') == $kupac->id ? 'selected' : '' }}
                                                        >{{ $kupac->naziv }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('kupac'))
                                                    <small class="text-danger">{{ $errors->first('kupac') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="prevoznik-input" class="col-sm-3 form-control-label">
                                                {{ __('Prevoznik') }}
                                            </label>
                                            <div class="col-sm-9">
                                                <select
                                                    name="prevoznik_id"
                                                    id="prevoznik-input"
                                                    class="form-control"
                                                >
                                                    <option disabled selected>Odaberite prevoznika</option>
                                                    @foreach($prevoznici as $prevoznik)
                                                        <option 
                                                            value="{{ $prevoznik->id }}"
                                                            {{ old('prevoznik_id') == $prevoznik->id ? 'selected' : '' }}
                                                        >{{ $prevoznik->naziv }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('prevoznik'))
                                                    <small class="text-danger">{{ $errors->first('prevoznik') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="rabat-input" class="col-sm-3 form-control-label">{{ __('Rabat') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="number"
                                                    name="rabat"
                                                    class="form-control {{ $errors->has('rabat') ? 'is-invalid' : '' }}"
                                                    id="rabat-input"
                                                    placeholder="{{ __('Unesite rabat') }}"
                                                    value="{{ old('rabat') }}"
                                                    step=".01"
                                                >
                                                @if($errors->has('rabat'))
                                                    <small class="text-danger">{{ $errors->first('rabat') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="datum-isporuke-input" class="col-sm-3 form-control-label">{{ __('Datum isporuke') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="datetime-local"
                                                    min="2019-01-01T00:00" 
                                                    max="2030-01-14T00:00"
                                                    step="1"
                                                    name="datum_isporuke"
                                                    class="form-control {{ $errors->has('datum_isporuke') ? 'is-invalid' : '' }}"
                                                    id="datum-isporuke-input"
                                                    placeholder="{{ __('Unesite datum isporuke') }}"
                                                    value="{{ old('datum_isporuke') }}"
                                                >
                                                @if($errors->has('datum_isporuke'))
                                                    <small class="text-danger">{{ $errors->first('datum_isporuke') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="datum-porudzbine-input" class="col-sm-3 form-control-label">{{ __('Datum porudzbine') }}</label>
                                            <div class="col-sm-9">
                                                <input
                                                    type="datetime-local"
                                                    min="2019-01-01T00:00" 
                                                    max="2030-01-14T00:00"
                                                    step="1"
                                                    name="datum_porudzbine"
                                                    class="form-control {{ $errors->has('datum_porudzbine') ? 'is-invalid' : '' }}"
                                                    id="datum-porudzbine-input"
                                                    placeholder="{{ __('Unesite datum porudzbine') }}"
                                                    value="{{ old('datum_porudzbine') }}"
                                                >
                                                @if($errors->has('datum_porudzbine'))
                                                    <small class="text-danger">{{ $errors->first('datum_porudzbine') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="napomena-input" class="col-sm-3 form-control-label">{{ __('Napomena') }}</label>
                                            <div class="col-sm-9">
                                                <textarea
                                                    name="napomena"
                                                    class="form-control {{ $errors->has('napomena') ? 'is-invalid' : '' }}"
                                                    id="napomena-input"
                                                    placeholder="{{ __('Unesite napomenu') }}"
                                                >{{ old('napomena') }}</textarea>
                                                @if($errors->has('napomena'))
                                                    <small class="text-danger">{{ $errors->first('napomena') }}</small>
                                                @endif
                                            </div>
                                        </div>
                                        <button class="float-right btn btn-primary">{{ __('Submit') }}</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('external-js')
<script type="text/javascript">
$(document).ready(function() {
    $('#add-new-form').on('click', function (e) {
        e.preventDefault();

        $('#stavka-forms').children().children().clone().appendTo('#append-forms');
    });
});
</script>
@endsection
