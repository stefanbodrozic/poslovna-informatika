@extends('layouts.master')
@section('title', __('Narudžbenice'))

@section('external-css')
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/datatables-net/media/css/dataTables.bootstrap4.min.css') }}"> <!-- original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/datatables-net/datatables.min.css') }}"> <!-- customization -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/select2/css/select2.min.css') }}"> <!-- Original -->
<link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/libs/select2/select2.min.css') }}"> <!-- Customization -->
@endsection
@section('content')
<div class="ks-column ks-page">
    <div class="ks-page-header">
        <section class="ks-title">
            <h3>{{ __('Narudžbenice') }}</h3>
            
            <div class="ks-controls">

                <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
            </div>
        </section>
    </div>
    <div class="ks-page-content">
        <div class="ks-page-content-body ks-invoices ks-body-wrap">
            <div class="ks-body-wrap-container">
                <div class="ks-full-table">
                    <div class="ks-full-table-header">
                        <h4 class="ks-full-table-name">{{ __('Napravljene narudžbenice')}}</h4>
                        <div class="ks-controls">
                            @if($poslovna_godina->zakljucena)
                                <a href="#">
                                    <button class="btn btn-danger">
                                        Poslovna godina {{ $poslovna_godina->godina }}. je zaključena
                                    </button>
                                </a>
                            @else
                                <a href="{{ route('narudžbenice.create') }}">
                                    <button class="btn btn-primary">{{ __('Napravite narudžbenicu')}}</button>
                                </a>
                            @endif
                        </div>
                    </div>
                    <table id="narudzbenice-table" class="table ks-table-info dt-responsive nowrap">
                        <thead>
                            <tr>
                                <th width="1">ID </th>
                                <th>{{ __('Broj') }}</th>
                                <th>{{ __('Kupac') }}</th>
                                <th>{{ __('Rabat') }}</th>
                                <th>{{ __('Datum porudzbine') }}</th>
                                <th>{{ __('Datum isporuke') }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('external-js')
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/datatables-net/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('theme/lc-admin/libs/select2/js/select2.min.js') }}"></script>
<script type="application/javascript">
var DatatablesDataSourceAjaxServer = {
    init: function () {
        $("#narudzbenice-table").DataTable({
            responsive: true, 
            searchDelay: 500, 
            processing: true, 
            serverSide: true, 
            ajax:"/narudžbenice", 
            columns: [
                { data: "id" }, 
                { data: "broj" },
                { data: "kupac.naziv" },
                { data: "rabat" },
                { data: "datum_porudzbine" },
                { data: "datum_isporuke" }
            ]
        })
    }
};

jQuery(document).ready(function () {
    DatatablesDataSourceAjaxServer.init();

    $('body').delegate('.delete-group', 'click', function (e) {
        e.preventDefault();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var id = $(this).data('id');
        var name = $(this).data('name');
        var table = $("#groups-table").DataTable();
        var tr = $(this).parents('tr');

        $.confirm({
            title: 'Pažnja!',
            content: 'Da li želite da obrišete ' + name + '?',
            type: 'danger',
            buttons: {
                confirm: {
                    text: 'OK',
                    btnClass: 'btn-danger',
                    action: function () {
                        $.ajax({
                            url: '/narudžbenice/' + id,
                            type: 'DELETE',
                        })
                        .done(function(data) {
                            table.row(tr).remove().draw();
                            ajaxNotify(data);
                        })
                        .fail(function() {
                            alert('Dogodila se greška');
                        });
                    }
                },
                cancel: function () {

                }
            }
        });
    });
});

</script>
@endsection
