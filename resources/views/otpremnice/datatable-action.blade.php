<div class="ks-controls">
    <a href="{{ route('otpremnice.show', ['otpremnice' => $otpremnica]) }}" target="_blank">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-eye ks-icon"></span>
        </button>
    </a>
</div>
