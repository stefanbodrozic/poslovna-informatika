<div class="ks-controls">
    <a href="{{ route('fakture.show', ['fakture' => $faktura]) }}" target="_blank">
        <button type="button" class="btn btn-primary ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-eye ks-icon"></span>
        </button>
    </a>
    <a href="{{ route('faktura.download', ['faktura' => $faktura]) }}" target="_blank">
        <button type="button" class="btn btn-success ks-light ks-no-text" style="line-height: 38px;">
            <span class="la la-download ks-icon"></span>
        </button>
    </a>
</div>
