<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/line-awesome/css/line-awesome.min.css') }}">
    <!--<link rel="stylesheet" type="text/css" href="assets/fonts/open-sans/styles.css">-->

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/fonts/montserrat/styles.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/tether/css/tether.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.jscrollpane.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/libs/flag-icon-css/css/flag-icon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/common.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/assets/select2.min.css') }}" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/jquery-confirm/jquery-confirm.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/flatpickr/flatpickr.min.css') }}">
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/themes/primary.min.css') }}">
    <link class="ks-sidebar-dark-style" rel="stylesheet" type="text/css" href="{{ asset('theme/lc-admin/assets/styles/themes/sidebar-black.min.css') }}">
    <link rel="stylesheet" href="{{ asset('theme/lc-admin/libs/notify/bootstrap-notify.min.css') }}">
</head>
<body>
    <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>{{ __('Odaberite poslovnu godinu i preduzece') }}</h3>

                <div class="ks-controls">

                    <button class="btn btn-outline-primary ks-light ks-content-nav-toggle" data-block-toggle=".ks-content-nav > .ks-nav">Menu</button>
                </div>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body ks-invoices ks-body-wrap">
                <div class="ks-nav-body-wrapper">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-5 ks-panels-column-section">
                                <div class="card">
                                    <div class="card-block">
                                        <form action="{{ url('start') }}" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="poslovna-godina-input" class="col-sm-3 form-control-label">{{ __('Poslovna godina') }}</label>
                                                <div class="col-sm-9">
                                                    <select 
                                                        name="poslovna_godina_id" 
                                                        class="form-control" 
                                                        id="poslovna-godina-input"
                                                        required 
                                                    >
                                                        <option value="" disabled selected>Odaberite poslovnu godinu</option>
                                                        @foreach($poslovne_godine as $pg)
                                                            <option 
                                                                value="{{ $pg->id }}"
                                                            >{{ $pg->godina }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="preduzece-input" class="col-sm-3 form-control-label">{{ __('Preduzece') }}</label>
                                                <div class="col-sm-9">
                                                    <select 
                                                        name="preduzece_id" 
                                                        class="form-control" 
                                                        id="preduzece-input"
                                                        required 
                                                    >
                                                        <option value="" disabled selected>Odaberite preduzece</option>
                                                        @foreach($preduzeca as $p)
                                                            <option value="{{ $p->id }}">{{ $p->naziv }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary pull-right" type="submit">Nastavite</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('theme/lc-admin/libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/popper/popper.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/responsejs/response.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/loading-overlay/loadingoverlay.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/tether/js/tether.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.jscrollpane.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/jscrollpane/jquery.mousewheel.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/flexibility/flexibility.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/velocity/velocity.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/jquery-confirm/jquery-confirm.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/notify/bootstrap-notify.min.js') }}"></script>
    <script src="{{ asset('theme/lc-admin/libs/flatpickr/flatpickr.min.js') }}"></script>
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="{{ asset('theme/lc-admin/assets/scripts/common.min.js') }}"></script>
    <script type='text/javascript' src="{{ asset('theme/lc-admin/assets/scripts/select2.full.min.js') }}"></script>
</body>
</html>
