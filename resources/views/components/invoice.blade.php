<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>{{ 'Faktura #' . $faktura->id }}</title>
    
    <style>
    .invoice-box {
        max-width: 1000px;
        margin: auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        background: #eee;
        border-bottom: 1px solid #ddd;
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    </style>
</head>

<body>
    <div class="invoice-box">
        <table cellpadding="0" cellspacing="0">
            <tr class="top">
                <td colspan="2">
                    <table>
                        <tr>
                            <td class="title">
                                <img src="{{ asset($preduzece->logo) }}" style="width:100%; max-width:300px;">
                            </td>
                            
                            <td>
                                <strong>Faktura #:</strong> {{ $faktura->broj }}<br>
                                <strong>Poslovna godina:</strong> {{ $faktura->poslovnaGodina->godina }}<br>
                                <strong>Napravljena:</strong> {{ \Carbon\Carbon::parse($faktura->datum_fakture)->format('d.m.Y') }}<br>
                                <strong>Platiti do:</strong> {{ \Carbon\Carbon::parse($faktura->platiti_do)->format('d.m.Y') }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr class="information">
                <td colspan="2">
                    <table>
                        <tr>
                            <td>
                                {{ $preduzece->naziv }}<br>
                                {{ $preduzece->adresa }}<br>
                                {{ $preduzece->mesto->grad }}
                            </td>
                            
                            <td>
                                <strong>Kupac:</strong> {{ $kupac->naziv }}<br>
                                <strong>JMBG:</strong> {{ $kupac->jmbg }}<br>
                                {{ $kupac->adresa }}<br>
                                {{ $kupac->mesto->naziv }}
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>            
            <tr class="heading">
                <td>R. Br.</td>
                <td>Naziv</td>
                <td>JM</td>
                <td>Količina</td>
                <td>Cena</td>
                <td>Vrednost</td>
                <td>Rabat %</td>
                <td>Iznos rabata</td>
                <td>Osnovica PDV</td>
                <td>Stopa PDV</td>
                <td>Iznos PDV</td>
                <td>Prodajna vrednost</td>
            </tr>
            
            @foreach($roba as $stavka)
                <tr class="item">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $stavka->naziv }}</td>
                    <td>{{ $stavka->jedinica_mere }}</td>
                    <td>{{ $stavka->pivot->kolicina }}</td>
                    <td>{{ $stavka->pivot->cena }}</td>
                    <td>{{ $stavka->pivot->vrednost }}</td>
                    <td>{{ $faktura->rabat }}</td>
                    <td>{{ $stavka->pivot->rabat_iznos }}</td>
                    <td>{{ $stavka->pivot->osnovica_pdv }}</td>
                    <td>{{ $stavka->pivot->stopa_pdv }}</td>
                    <td>{{ $stavka->pivot->iznos_pdv }}</td>
                    <td>{{ $stavka->pivot->prodajna_vrednost }}</td>
                </tr>
            @endforeach
            
            <tr class="total">
                <td>
                    <strong>Ukupan porez: {{ $faktura->ukupan_porez }}</strong>
                </td>
                <td>
                    <strong>Ukupan rabat: {{ $faktura->ukupan_rabat }}</strong>
                </td>
                <td>
                   <strong>Ukupan iznos: {{ $faktura->ukupan_iznos }}</strong>
                </td>
            </tr>
            <tr>
                <td>
                    <strong>Račun:</strong> {{ $preduzece->racun }}
                </td>
                <td>
                    <strong>Br. telefona:</strong> {{ $preduzece->telefon }}
                </td>
                <td>
                    {{ $faktura->napomena }}
                </td>
            </tr>
            <tr>
                <td>
                    <img src="{{ asset($preduzece->potpis) }}" alt="Potpis">
                </td>
            </tr>
            <p>
            </p>
        </table>
    </div>
</body>
</html>
