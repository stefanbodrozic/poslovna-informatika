    <!-- BEGIN DEFAULT SIDEBAR -->
<div class="ks-column ks-sidebar ks-info">
    <div class="ks-wrapper ks-sidebar-wrapper">
        <ul class="nav nav-pills nav-stacked">

            <li class="nav-item">
                <a class="nav-link"  href="{{ url('/') }}" >
                    <span class="ks-icon la la-dashboard"></span>
                    <span>{{ __('Početna') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('narudžbenice.index') }}">
                    <span class="ks-icon la la-folder-open"></span>
                    <span>{{ __('Narudžbenice') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('fakture.index') }}">
                    <span class="ks-icon la la-file-o"></span>
                    <span>{{ __('Fakture') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('otpremnice.index') }}">
                    <span class="ks-icon la la-file-text-o"></span>
                    <span>{{ __('Otpremnice') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('roba.index') }}">
                    <span class="ks-icon la la-industry"></span>
                    <span>{{ __('Roba') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('grupa-robe.index') }}">
                    <span class="ks-icon la la-archive"></span>
                    <span>{{ __('Grupa robe') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('pdv.index') }}">
                    <span class="ks-icon la la-bank"></span>
                    <span>{{ __('PDV') }}</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ route('cenovnici.index') }}">
                    <span class="ks-icon la la-money"></span>
                    <span>{{ __('Cenovnici') }}</span>
                </a>
            </li>
        </ul>
    </div>
</div>
<!-- END DEFAULT SIDEBAR -->
