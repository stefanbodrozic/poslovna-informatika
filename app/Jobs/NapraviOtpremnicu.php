<?php

namespace App\Jobs;

use App\Faktura;
use App\Otpremnica;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class NapraviOtpremnicu implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $faktura;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Faktura $faktura)
    {
        $this->faktura = $faktura;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $otpremnica = Otpremnica::create([
            'broj' => $this->faktura->broj,
            'ukupan_iznos' => $this->faktura->ukupan_iznos,
            'kupac_id' => $this->faktura->kupac_id,
            'prevoznik_id' => $this->faktura->prevoznik_id,
            'preduzece_id' => $this->faktura->preduzece_id,
            'faktura_id' => $this->faktura->id,
            'user_id' => Auth::id(),
            'poslovna_godina_id' => $this->faktura->poslovna_godina_id,
            'datum_porudzbine' => $this->faktura->datum_fakture,
            'datum_isporuke' => $this->faktura->datum_isporuke
        ]);

        foreach ($this->faktura->stavke as $stavka) {
            $otpremnica->stavke()->attach(
                $stavka->id,
                [
                    'kolicina' => $stavka->pivot->kolicina,
                    'prodajna_vrednost' => $stavka->pivot->prodajna_vrednost
                ]
            );
        }
    }
}
