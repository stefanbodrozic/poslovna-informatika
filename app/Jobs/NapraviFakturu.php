<?php

namespace App\Jobs;

use App\Cenovnik;
use App\Faktura;
use App\Jobs\NapraviOtpremnicu;
use App\Narudzbenica;
use App\Pdv;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class NapraviFakturu implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $narudzbenica;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Narudzbenica $narudzbenica)
    {
        $this->narudzbenica = $narudzbenica;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $platiti_do = new Carbon($this->narudzbenica->datum_porudzbine);

        $cenovnik = Cenovnik::where(
                    'created_at', '<=', $this->narudzbenica->datum_porudzbine
                )->first();

        if (! $cenovnik) {
            $cenovnik = Cenovnik::where(
                    'created_at', '>', $this->narudzbenica->datum_porudzbine
                )->first();
        }

        $ukupan_iznos = 0.00;
        $ukupan_porez = 0.00;
        $ukupan_rabat = 0.00;
        foreach ($this->narudzbenica->stavke as $stavka) {
            $cena = $cenovnik->stavke->find($stavka)->pivot->cena;

            $pdv = $stavka->grupaRobe->pdv;
            $pdvStopa = optional($pdv->stope()
                    ->where(
                        'created_at', '<=', $this->narudzbenica->datum_porudzbine
                    )->first()
                )->procenat;

            if (! $pdvStopa) {
                $pdvStopa = $pdv->stope()
                    ->where(
                        'created_at', '>', $this->narudzbenica->datum_porudzbine
                    )->first()->procenat;
            }

            $kolicina = $stavka->pivot->kolicina;
            $vrednost = $kolicina * $cena;
            $rabat_iznos = $vrednost * $this->narudzbenica->rabat / 100;
            $osnovica_pdv = $vrednost - $rabat_iznos;
            $stopa_pdv = $pdvStopa;
            $iznos_pdv = $osnovica_pdv * $stopa_pdv / 100;
            $prodajna_vrednost = $vrednost - $rabat_iznos + $iznos_pdv;

            $ukupan_porez += $iznos_pdv;
            $ukupan_rabat += $rabat_iznos;
            $ukupan_iznos += $prodajna_vrednost;
        }

        $faktura = Faktura::create([
            'broj' => $this->narudzbenica->broj,
            'ukupan_iznos' => $ukupan_iznos,
            'ukupan_porez' => $ukupan_porez,
            'ukupan_rabat' => $ukupan_rabat,
            'rabat' => $this->narudzbenica->rabat,
            'kupac_id' => $this->narudzbenica->kupac_id,
            'preduzece_id' => $this->narudzbenica->preduzece_id,
            'prevoznik_id' => $this->narudzbenica->prevoznik_id,
            'poslovna_godina_id' => $this->narudzbenica->poslovna_godina_id,
            'narudzbenica_id' => $this->narudzbenica->id,
            'user_id' => Auth::id(),
            'platiti_do' => $platiti_do->addDays(20),
            'datum_fakture' => $this->narudzbenica->datum_porudzbine,
            'datum_isporuke' => $this->narudzbenica->datum_isporuke
        ]);

        foreach ($this->narudzbenica->stavke as $stavka) {
            $cena = $cenovnik->stavke->find($stavka)->pivot->cena;

            $pdv = $stavka->grupaRobe->pdv;
            $pdvStopa = optional($pdv->stope()
                    ->where(
                        'created_at', '<=', $this->narudzbenica->datum_porudzbine
                    )->first()
                )->procenat;

            if (! $pdvStopa) {
                $pdvStopa = $pdv->stope()
                    ->where(
                        'created_at', '>', $this->narudzbenica->datum_porudzbine
                    )->first()->procenat;
            }

            $kolicina = $stavka->pivot->kolicina;
            $vrednost = $kolicina * $cena;
            $rabat_iznos = $vrednost * $faktura->rabat / 100;
            $osnovica_pdv = $vrednost - $rabat_iznos;
            $stopa_pdv = $pdvStopa;
            $iznos_pdv = $osnovica_pdv * $stopa_pdv / 100;
            $prodajna_vrednost = $vrednost - $rabat_iznos + $iznos_pdv;

            $faktura->stavke()->attach(
                $stavka->id, [
                    'kolicina' => $kolicina,
                    'cena' => $cena,
                    'vrednost' => $vrednost, 
                    'rabat_iznos' => $rabat_iznos, 
                    'osnovica_pdv' => $osnovica_pdv, 
                    'stopa_pdv' => $stopa_pdv,
                    'iznos_pdv' => $iznos_pdv, 
                    'prodajna_vrednost' => $prodajna_vrednost
                ]
            );
        }

        dispatch(new NapraviOtpremnicu($faktura));
    }
}
