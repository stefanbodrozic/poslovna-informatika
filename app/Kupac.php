<?php

namespace App;

use App\Mesto;
use App\Preduzece;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kupac extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kupci';

    public function mesto()
    {
        return $this->belongsTo(Mesto::class);
    }

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }
}
