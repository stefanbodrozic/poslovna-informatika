<?php

namespace App;

use App\Kupac;
use App\Narudzbenica;
use App\Pdv;
use App\PoslovnaGodina;
use App\Preduzece;
use App\Roba;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Faktura extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fakture';

    public function pdv() 
    {
        return $this->belongsTo(Pdv::class);
    }

    public function kupac()
    {
        return $this->belongsTo(Kupac::class);
    }

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }

    public function narudzbenica()
    {
        return $this->belongsTo(Narudzbenica::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function poslovnaGodina()
    {
        return $this->belongsTo(PoslovnaGodina::class);
    }

    public function stavke()
    {
        return $this->belongsToMany(Roba::class, 'faktura_stavka', 'faktura_id', 'stavka_id')
                    ->withPivot(
                        'kolicina', 'cena', 'vrednost', 'rabat_iznos', 
                        'osnovica_pdv', 'stopa_pdv', 'iznos_pdv', 'prodajna_vrednost'
                    );

            
            
            
            
    }
}
