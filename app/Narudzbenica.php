<?php

namespace App;

use App\Kupac;
use App\Pdv;
use App\Preduzece;
use App\Prevoznik;
use App\Roba;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Narudzbenica extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'narudzbenice';

    public function pdv() 
    {
        return $this->belongsTo(Pdv::class);
    }

    public function kupac()
    {
        return $this->belongsTo(Kupac::class);
    }

    public function prevoznik()
    {
        return $this->belongsTo(Prevoznik::class);
    }

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stavke()
    {
        return $this->belongsToMany(Roba::class, 'narudzbenica_stavka', 'narudzbenica_id', 'stavka_id')
                    ->withPivot('kolicina');
    }
}
