<?php

namespace App;

use App\Mesto;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Preduzece extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'preduzeca';

    public function mesto()
    {
        return $this->belongsTo(Mesto::class);
    }
}
