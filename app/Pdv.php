<?php

namespace App;

use App\PdvStopa;
use Illuminate\Database\Eloquent\Model;

class Pdv extends Model
{
    protected $guarded = [];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pdv';

    public function stope()
    {
        return $this->hasMany(PdvStopa::class);
    }

    public function aktivnaStopa() 
    {
        return $this->hasOne(PdvStopa::class)
                    ->where('aktivan', '1');
    }
}
