<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CenovnikRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aktivan' => 'required',
            'naziv' => 'required|string|unique:cenovnici',
            'roba' => 'required|array'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'preduzece_id' => session()->get('preduzece_id')
        ]);
    }
}
