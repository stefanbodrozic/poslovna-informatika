<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PdvRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'naziv' => 'required|string',
            'procenat' => 'required|between:0.01,99.99',
            'vazi_do' => 'required'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'vazi_do' => str_replace('T', ' ', $this->input('vazi_do'))
        ]);
    }
}
