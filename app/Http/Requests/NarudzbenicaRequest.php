<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class NarudzbenicaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roba_kolicina' => 'required|array',
            'roba_kolicina.*' => 'required|numeric|min:1',
            'roba_id' => 'required|array',
            'rabat' => 'required|numeric|between:0.00,100.00',
            'datum_isporuke' => 'required|after:' . Carbon::now(),
            'datum_porudzbine' => 'required|before_or_equal:' . Carbon::now(),
            'napomena' => 'nullable|string'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    public function prepareForValidation()
    {
        $this->merge([
            'datum_isporuke' => str_replace('T', ' ', $this->input('datum_isporuke')),
            'datum_porudzbine' => str_replace('T', ' ', $this->input('datum_porudzbine')),
            'user_id' => Auth::id(),
            'preduzece_id' => session()->get('preduzece_id'),
            'poslovna_godina_id' => session()->get('poslovna_godina_id'),
        ]);
    }
}
