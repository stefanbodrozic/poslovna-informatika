<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RobaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'naziv' => 'required|string|min:3|max:100',
            'grupa_robe_id' => 'required|integer',
            'jedinica_mere' => 'required|string',
            'cena' => 'required|regex:/^\d*(\.\d{1,2})?$/'
        ];
    }
}
