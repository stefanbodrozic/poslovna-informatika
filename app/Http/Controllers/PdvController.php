<?php

namespace App\Http\Controllers;

use App\Http\Requests\PdvRequest;
use App\Pdv;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class PdvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Pdv::query()
                        ->with('aktivnaStopa')
                )->addColumn('action', function ($pdv) {
                   return view('pdv.datatable-action', compact('pdv'));
               })->make();
        }

        return view('pdv.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('pdv.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PdvRequest $request)
    {
        $pdv = Pdv::create($request->only('naziv'));

        $pdv->stope()
            ->create($request->only('procenat', 'vazi_do'));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pdv  $pdv
     * @return \Illuminate\Http\Response
     */
    public function show(Pdv $pdv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pdv  $pdv
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdv $pdv)
    {
        return view('pdv.edit')->with(
            'pdv', $pdv
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pdv  $pdv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pdv $pdv)
    {
        $pdv->stope()
            ->where('aktivan', 1)
            ->update(['aktivan' => 0]);
        
        $pdv->stope()
            ->create($request->all());

        return redirect()->route('pdv.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pdv  $pdv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pdv $pdv)
    {
        //
    }
}
