<?php

namespace App\Http\Controllers;

use App\PoslovnaGodina;
use App\Preduzece;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('dashboard');
    }

    public function start() 
    {
        return view('getting_started')->with([
            'poslovne_godine' => PoslovnaGodina::all(),
            'preduzeca' => Preduzece::all(),
        ]);
    }

    public function starting(Request $request) 
    {
        session([
            'poslovna_godina_id' => $request->poslovna_godina_id,
            'preduzece_id' => $request->preduzece_id
        ]);

        return redirect('/');
    }
}
