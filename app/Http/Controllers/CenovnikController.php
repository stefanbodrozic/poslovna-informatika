<?php

namespace App\Http\Controllers;

use App\Cenovnik;
use App\Http\Requests\CenovnikRequest;
use App\Roba;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class CenovnikController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Cenovnik::query()
                )->addColumn('action', function ($cenovnik) {
                   return view('cenovnici.datatable-action', compact('cenovnik'));
               })->make();
        }

        return view('cenovnici.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cenovnici.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CenovnikRequest $request)
    {
        if ($request->aktivan) {
            Cenovnik::where('aktivan', 1)
                ->update(['aktivan' => 0]);
        }

        $cenovnik = Cenovnik::create($request->except('roba'));

        foreach ($request->roba as $stavka) {
            $cenovnik->stavke()
                ->attach(
                    $stavka['id'],
                    ['cena' => $stavka['cena']]
                );
        }

        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cenovnik  $cenovnik
     * @return \Illuminate\Http\Response
     */
    public function show(Cenovnik $cenovnici)
    {
        $roba = Roba::query()
                ->whereHas('cenovnici', function ($q) use ($cenovnici) {
                    $q->where('id', $cenovnici->id);
                })->with([
                    'grupaRobe',
                    'cenovnici' => function ($q) use ($cenovnici) {
                        $q->where('id', $cenovnici->id);
                    }
                ]);

        if (request()->ajax()) {
            return DataTables::eloquent($roba)
                ->make();
        }

        return view('cenovnici.edit')->with([
            'cenovnik' => $cenovnici,
            'roba' => $cenovnici->stavke
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cenovnik  $cenovnik
     * @return \Illuminate\Http\Response
     */
    public function edit(Cenovnik $cenovnici)
    {
        $roba = $cenovnici->stavke
                ->load([
                    'grupaRobe',
                    'cenovnici' => function ($q) use ($cenovnici) {
                        $q->where('id', $cenovnici->id);
                    }
                ]);

        return view('cenovnici.create')->with([
            'stari_cenovnik' => $cenovnici,
            'roba' => $roba
        ]);
    }

    public function toggleActive(Request $request, Cenovnik $cenovnik) 
    {
        if ($cenovnik->aktivan) {
            return response()->json(false);
        }
        
        Cenovnik::where('aktivan', 1)
                ->update(['aktivan' => 0]);

        $cenovnik->update(['aktivan' => 1]);

        return response()->json(true);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cenovnik  $cenovnik
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cenovnik $cenovnik)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cenovnik  $cenovnik
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cenovnik $cenovnik)
    {
        //
    }
}
