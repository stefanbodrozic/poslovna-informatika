<?php

namespace App\Http\Controllers;

use App\Cenovnik;
use App\GrupaRobe;
use App\Http\Requests\RobaRequest;
use App\Roba;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class RobaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Roba::query()
                        ->with('grupaRobe')
                )->addColumn('action', function ($roba) {
                   return view('roba.datatable-action', compact('roba'));
               })->make();
        }

        return view('roba.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roba.create')->with([
            'grupa_robe' => GrupaRobe::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RobaRequest $request)
    {
        $stavka = Roba::create($request->except('cena'));
        
        $cenovnik = Cenovnik::where('aktivan', 1)->first();

        $cenovnik->stavke()
            ->attach($stavka->id, ['cena' => $request->cena]);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Roba  $roba
     * @return \Illuminate\Http\Response
     */
    public function show(Roba $roba)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Roba  $roba
     * @return \Illuminate\Http\Response
     */
    public function edit(Roba $roba)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Roba  $roba
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Roba $roba)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Roba  $roba
     * @return \Illuminate\Http\Response
     */
    public function destroy(Roba $roba)
    {
        //
    }
}
