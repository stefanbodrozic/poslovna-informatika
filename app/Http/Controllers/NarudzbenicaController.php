<?php

namespace App\Http\Controllers;

use App\Http\Requests\NarudzbenicaRequest;
use App\Jobs\NapraviFakturu;
use App\Kupac;
use App\Narudzbenica;
use App\PoslovnaGodina;
use App\Prevoznik;
use App\Roba;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class NarudzbenicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Narudzbenica::query()
                        ->with('kupac')
                )->addColumn('action', function ($narudzbenica) {
                   return view('narudzbenice.datatable-action', compact('narudzbenica'));
               })->make();
        }

        return view('narudzbenice.index')->with(
            'poslovna_godina', PoslovnaGodina::find(session()->get('poslovna_godina_id'))
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (PoslovnaGodina::find(session()->get('poslovna_godina_id'))->zakljucena) {
            return redirect()->route('narudžbenice.index');
        }

        return view('narudzbenice.create')->with([
            'kupci' => Kupac::all(),
            'prevoznici' => Prevoznik::all(),
            'roba' => Roba::with('grupaRobe')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NarudzbenicaRequest $request)
    {
        $poslovna_godina = PoslovnaGodina::find($request->poslovna_godina_id);

        $n = Narudzbenica::where('poslovna_godina_id', $poslovna_godina->id)
                ->orderBy('broj', 'desc')
                ->first();

        if (! $n) {
            $broj = 1;
        } else {
            $broj = (int) ++$n->broj;
        }

        $narudzbenica = Narudzbenica::create(
            array_merge(
                $request->except('roba_kolicina', 'roba_id'),
                ['broj' => $broj]
            )
        );

        foreach ($request->roba_id as $key => $roba) {
            $narudzbenica->stavke()->attach($roba, ['kolicina' => $request->roba_kolicina[$key]]);
        }

        dispatch(new NapraviFakturu($narudzbenica));

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Narudzbenica  $narudzbenica
     * @return \Illuminate\Http\Response
     */
    public function show(Narudzbenica $narudzbenica)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Narudzbenica  $narudzbenica
     * @return \Illuminate\Http\Response
     */
    public function edit(Narudzbenica $narudzbenica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Narudzbenica  $narudzbenica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Narudzbenica $narudzbenica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Narudzbenica  $narudzbenica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Narudzbenica $narudzbenica)
    {
        //
    }
}
