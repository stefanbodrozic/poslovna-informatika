<?php

namespace App\Http\Controllers;

use App\Faktura;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class FakturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Faktura::query()
                        ->with('kupac', 'narudzbenica', 'poslovnaGodina')
                )->addColumn('action', function ($faktura) {
                   return view('fakture.datatable-action', compact('faktura'));
               })->make();
        }

        return view('fakture.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faktura  $faktura
     * @return \Illuminate\Http\Response
     */
    public function show(Faktura $fakture)
    {
        $faktura = $fakture->load('preduzece', 'kupac', 'stavke', 'poslovnaGodina');

        return view('components.invoice')->with([
            'faktura' => $faktura,
            'preduzece' => $faktura->preduzece->load('mesto'),
            'kupac' => $faktura->kupac,
            'roba' => $faktura->stavke
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faktura  $faktura
     * @return \Illuminate\Http\Response
     */
    public function edit(Faktura $faktura)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faktura  $faktura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faktura $faktura)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faktura  $faktura
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faktura $faktura)
    {
        //
    }

    public function download(Faktura $faktura) 
    {
        return response()->xml(
            $faktura->load(
                'stavke:id,naziv,jedinica_mere', 
                'preduzece:id,naziv,pib,adresa,email,mesto_id',
                'preduzece.mesto:id,grad,postanski_broj',
                'kupac:id,naziv,adresa,pib,jmbg'
            )
        );
    }
}
