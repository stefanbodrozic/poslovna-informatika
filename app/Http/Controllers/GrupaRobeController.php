<?php

namespace App\Http\Controllers;

use App\GrupaRobe;
use App\Http\Requests\GrupaRobeRequest;
use App\Pdv;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class GrupaRobeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    GrupaRobe::query()
                        ->with('pdv', 'pdv.aktivnaStopa')
                )->addColumn('action', function ($grupaRobe) {
                   return view('grupa_robe.datatable-action', compact('grupaRobe'));
               })->make();
        }

        return view('grupa_robe.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('grupa_robe.create')->with([
            'pdvs' => Pdv::with('aktivnaStopa')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GrupaRobeRequest $request)
    {
        GrupaRobe::create($request->all());

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GrupaRobe  $grupaRobe
     * @return \Illuminate\Http\Response
     */
    public function show(GrupaRobe $grupaRobe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GrupaRobe  $grupaRobe
     * @return \Illuminate\Http\Response
     */
    public function edit(GrupaRobe $grupaRobe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GrupaRobe  $grupaRobe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GrupaRobe $grupaRobe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GrupaRobe  $grupaRobe
     * @return \Illuminate\Http\Response
     */
    public function destroy(GrupaRobe $grupaRobe)
    {
        //
    }
}
