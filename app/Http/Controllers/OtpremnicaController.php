<?php

namespace App\Http\Controllers;

use App\Otpremnica;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class OtpremnicaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            return DataTables::eloquent(
                    Otpremnica::query()
                        ->with('kupac', 'poslovnaGodina', 'prevoznik')
                )->addColumn('action', function ($otpremnica) {
                   return view('otpremnice.datatable-action', compact('otpremnica'));
               })->make();
        }

        return view('otpremnice.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Otpremnica  $otpremnica
     * @return \Illuminate\Http\Response
     */
    public function show(Otpremnica $otpremnice)
    {
        $otpremnica = $otpremnice->load('preduzece', 'kupac', 'stavke', 'poslovnaGodina');

        return view('components.delivery_note')->with([
            'otpremnica' => $otpremnica,
            'preduzece' => $otpremnica->preduzece->load('mesto'),
            'kupac' => $otpremnica->kupac,
            'roba' => $otpremnica->stavke
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Otpremnica  $otpremnica
     * @return \Illuminate\Http\Response
     */
    public function edit(Otpremnica $otpremnica)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Otpremnica  $otpremnica
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Otpremnica $otpremnica)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Otpremnica  $otpremnica
     * @return \Illuminate\Http\Response
     */
    public function destroy(Otpremnica $otpremnica)
    {
        //
    }
}
