<?php

namespace App;

use App\Cenovnik;
use App\Faktura;
use App\GrupaRobe;
use App\Narudzbenica;
use App\Otpremnica;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roba extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $hidden = ['pivot'];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'roba';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    // protected $appends = ['aktivna_cena'];

    public function grupaRobe() 
    {
        return $this->belongsTo(GrupaRobe::class);
    }

    // public function getAktivnaCenaAttribute()
    // {
    //     return Cenovnik::where('aktivan', 1)->first()->stavke()->find($this->id)->pivot->cena;
    // }

    public function aktivnaCena()
    {
        return Cenovnik::where('aktivan', 1)->first()->stavke()->find($this->id)->pivot->cena;
    }

    public function getStopaPdvAttribute() 
    {
        return $this->grupaRobe->pdv->aktivnaStopa->procenat;
    }

    public function cenovnici() 
    {
        return $this->belongsToMany(Cenovnik::class, 'cenovnik_stavka', 'stavka_id', 'cenovnik_id')
                    ->withPivot('cena');
    }

    public function fakture()
    {
        return $this->belongsToMany(Faktura::class, 'faktura_stavka')
                    ->withPivot(
                        'kolicina', 'cena', 'vrednost', 'rabat_iznos', 
                        'osnovica_pdv', 'stopa_pdv', 'iznos_pdv', 'prodajna_vrednost'
                    );
    }

    public function narudzbenice()
    {
        return $this->belongsToMany(Narudzbenica::class, 'narudzbenica_stavka')
                    ->withPivot('kolicina');
    }

    public function otpremnice()
    {
        return $this->belongsToMany(Otpremnica::class, 'otpremnica_stavka')
                    ->withPivot('kolicina');
    }
}
