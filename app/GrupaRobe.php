<?php

namespace App;

use App\Pdv;
use App\Preduzece;
use App\Roba;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GrupaRobe extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'grupe_robe';

    public function pdv()
    {
        return $this->belongsTo(Pdv::class);
    }

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }

    public function roba()
    {
        return $this->hasMany(Roba::class, 'grupa_robe_id');
    }
}
