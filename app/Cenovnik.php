<?php

namespace App;

use App\Preduzece;
use App\Roba;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cenovnik extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cenovnici';

    public function preduzece() 
    {
        return $this->belongsTo(Preduzece::class);
    }

    public function stavke() 
    {
        return $this->belongsToMany(Roba::class, 'cenovnik_stavka', 'cenovnik_id', 'stavka_id')
                    ->withPivot('cena');
    }
}
