<?php

namespace App;

use App\Faktura;
use App\PoslovnaGodina;
use App\Prevoznik;
use App\Roba;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Otpremnica extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'otpremnice';

    public function faktura() 
    {
        return $this->belongsTo(Faktura::class);
    }

    public function kupac()
    {
        return $this->belongsTo(Kupac::class);
    }

    public function prevoznik()
    {
        return $this->belongsTo(Prevoznik::class);
    }

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function poslovnaGodina()
    {
        return $this->belongsTo(PoslovnaGodina::class);
    }

    public function stavke()
    {
        return $this->belongsToMany(Roba::class, 'otpremnica_stavka', 'otpremnica_id', 'stavka_id')
                    ->withPivot('kolicina', 'prodajna_vrednost');
    }
}
