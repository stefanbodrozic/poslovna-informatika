<?php

namespace App;

use App\Pdv;
use Illuminate\Database\Eloquent\Model;

class PdvStopa extends Model
{
    protected $guarded = [];
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pdv_stope';

    public function pdv()
    {
        return $this->belongsTo(Pdv::class);
    }
}
