<?php

namespace App;

use App\Preduzece;
use Illuminate\Database\Eloquent\Model;

class PoslovnaGodina extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'poslovne_godine';

    public function preduzece()
    {
        return $this->belongsTo(Preduzece::class);
    }
}
